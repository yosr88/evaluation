# Évaluation

À fournir par équipe : un lien vers un dépôt GIT public dans
un répertoire `Evaluation`, avant le 29 janvier 2024.

## Déployer Drupal sur une seule instance

En s'inspirant du déploiement avec Tofu d'une instance hébergeant
Wordpress (TP_tf), fournir un déploiement de Drupal dans le Cloud.

Cf. https://www.rosehosting.com/blog/how-to-install-drupal-on-debian-12/

Attendu : le plan Tofu et un README décrivant l'architecture 
déployée et les consignes pour appliquer et tester ce déploiement.

## Déployer Drupal sur plusieurs instances

En s'inspirant du déploiement de plusieurs instances dans un VPC
(`TP_vpc`), fournir un déploiement de Drupal sur trois (ou plus)
instances (deux backends Web et une base de données).

Attendu : le plan Tofu et un README décrivant l'architecture 
déployée et les consignes pour appliquer et tester ce déploiement.


